# React Basic Sandbox

Taken from the [React Documentation](https://reactjs.org/docs/getting-started.html#online-playgrounds) and adapted to show how a basic example of both a class component and a functional component being rendered with React.js.

This should be enough for very simple React.js examples like component nesting, sharing props, setting states and testing hooks.

## Usage

Open `index.html` with your browser. Edit the script in the `body` tag and refresh to see the results.

If you wish to use the functional component comment the class component and uncomment the funcional component. Save and refresh the page on your browser.

## Note

The dependencies are met by using unpkg.com. Needless to say you need an internet connection to use this but... maybe I'll add the files directly here later.
